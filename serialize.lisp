#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:cl-mongo-meta)

(defmethod cl-mongo::bson-encode-container ((object bson-serializable)
                                            &key (size 32) (array nil))
  (let ((array (or array (cl-mongo::make-octet-vector size))))
    (cl-mongo::add-octets (cl-mongo::int32-to-octet 0) array)
    (loop :for class :in (c2mop:class-precedence-list (class-of object)) :do
         (loop
            :for slot :in (c2mop:class-direct-slots class) :do
              (cl-mongo::bson-encode-container slot :array array :object object)))
    (cl-mongo::normalize-array array)))

(defmethod cl-mongo::bson-encode ((key string) (value bson-serializable) &key)
  (cl-mongo::bson-encode key (cl-mongo::bson-encode-container value)))

(defmethod cl-mongo::bson-encode-container ((slot collection-slot) &key array object)
  (assert (not (null array)))
  (assert (not (null object)))
  (let ((slot-name (c2mop:slot-definition-name slot)))
    (when (and (not (ghostp slot))
               (slot-boundp object slot-name))
      (cl-mongo::add-octets
       (cl-mongo::bson-encode (key-name slot)
                              (serial-value object slot))
       array
       :start 4 :from-end 1)))
  array)
