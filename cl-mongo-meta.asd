;;;; Copyright (c) 2020 Gnuxie <Gnuxie@protonmail.com>
(asdf:defsystem #:cl-mongo-meta
  :description "Describe cl-mongo-meta here"
  :author "Gnuxie <Gnuxie@protonmail.com>"
  :license  "Artistic License 2.0"
  :version "0.0.1"
  :depends-on ("cl-mongo" "closer-mop")
  :serial t
  :components ((:file "package")
               (:file "protocol")
               (:file "cl-mongo-meta")
               (:file "serialize")))
