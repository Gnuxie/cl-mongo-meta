#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>

    This file contains modified code from postmodern dao-tables

    https://github.com/marijnh/Postmodern/blob/master/postmodern/table.lisp

    Which contains the following notice:
;;; Copyright (c) Marijn Haverbeke, marijnh@gmail.com
;;; 
;;; This software is provided 'as-is', without any express or implied
;;; warranty. In no event will the authors be held liable for any
;;; damages arising from the use of this software.
;;; 
;;; Permission is granted to anyone to use this software for any
;;; purpose, including commercial applications, and to alter it and
;;; redistribute it freely, subject to the following restrictions:
;;; 
;;; 1. The origin of this software must not be misrepresented; you must
;;;    not claim that you wrote the original software. If you use this
;;;    software in a product, an acknowledgment in the product
;;;    documentation would be appreciated but is not required.
;;; 
;;; 2. Altered source versions must be plainly marked as such, and must
;;;    not be misrepresented as being the original software.
;;; 
;;; 3. This notice may not be removed or altered from any source
;;;    distribution.

    The code in this file is licensed under the Artistic License 2.0
 
|#

(in-package #:cl-mongo-meta)

(defclass collection-class (mongo-class c2mop:standard-class)
  ((%collection-name :type string
                     :documentation
                     "use the :collection-name in the class options.")))

(defclass collection-slot (mongo-slot c2mop:standard-direct-slot-definition)
  ((%ghost :initarg :ghost
           :reader ghostp
           :type boolean
           :initform nil
           :documentation "The slot will not be written to a cl-mongo collection")

   (%key-name :initarg :key-name
              :reader key-name
              :type string
              :documentation "If unbound will transform the slot-name instead")

   (%serializer-reader :initarg :serializer-reader
                       :type (or symbol function)
                       :documentation "if provided this will be used by
serial-value to get the value from the slot.

Either a symbol naming a function, or a function.")))

;;;
;;; make a slot
;;; write protocol around that
;;; refer to lisp web book
;;; MIT so people can do whatever the fuck they want.
;;; think hard about indexes and object relations

(defmethod shared-initialize :before ((class collection-class) slot-names
                                      &key collection-name &allow-other-keys)
  (declare (ignore slot-names))
  (if collection-name
      (setf (slot-value class '%collection-name) (car collection-name))
      (slot-makunbound class '%collection-name)))

(defmethod collection-name ((collection-class collection-class))
  (if (slot-boundp collection-class '%collection-name)
      (slot-value collection-class '%collection-name)
      ;; will not work with things from the cl-mongo-meta package.
      (prin1-to-string (class-name collection-class))))

(defmethod collection-name ((collection bson-serializable))
  (collection-name (class-of collection)))

(defmethod c2mop:validate-superclass ((class collection-class)
                                      (super c2mop:standard-class))
  t)

(defmethod c2mop:validate-superclass ((class c2mop:standard-class)
                                      (super collection-class))
  t)

(defmethod c2mop:direct-slot-definition-class ((class collection-class)
                                               &rest initargs)
  (declare (ignore initargs))
  (find-class 'collection-slot))

(defmethod c2mop:class-direct-superclasses ((class collection-class))
  (append (remove (find-class 'standard-object) (call-next-method))
          (list (find-class 'bson-serializable)
                (find-class 'standard-object))))

(defmethod serial-value ((collection bson-serializable) (slot collection-slot))
  (if (slot-boundp slot '%serializer-reader)
      (let ((serializer-reader (slot-value slot '%serializer-reader)))
        (funcall
         (etypecase serializer-reader
           (symbol (symbol-function serializer-reader))
           (function serializer-reader))
         collection))
      ;; I can't get slot-value-using-class to work here?
      #+ (or)
      (c2mop:slot-value-using-class (class-of collection)
                                    collection
                                    slot)
      (slot-value collection (c2mop:slot-definition-name slot))
      ))

(defmethod db.insert ((collection bson-serializable) &key)
  (cl-mongo:db.insert (collection-name collection) collection))
