#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:cl-mongo-meta)

(defclass mongo-class () ())

(defgeneric collection-name (mongo-class)
  (:documentation "return the name of the collection for the class."))

(defclass mongo-slot () ())

(defgeneric ghostp (mongo-slot)
  (:documentation "Test if the slot is a ghost."))

(defgeneric key-name (mongo-slot)
  (:documentation "Get the BSON key name for the slot."))

(defgeneric serial-value (mongo-collection mongo-slot)
  (:documentation "This is to be called by the collection writer
to obtain the slot-value, this is done so the user can overide the way the value
is presented if they need to.

Note that there are smart ways to implement this so that accessors can still be
used

See the example implementation."))

(defclass bson-serializable () ())

(defgeneric db.insert (collection &key)
  (:documentation "insert the collection into the db."))
