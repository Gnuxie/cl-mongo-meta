;;;; package.lisp
;;
;;;; Copyright (c) 2020 Gnuxie <Gnuxie@protonmail.com>


(defpackage #:cl-mongo-meta
  (:use #:cl)
  (:export
   ;; protocol
   ;;; mongo-class
   #:mongo-class
   #:collection-name
   #:bson-serializable
   ;;; mongo-slot
   #:mongo-slot
   #:ghostp
   #:key-name
   #:serial-value
   ;;; operations
   #:db.insert
   ;; cl-mongo-meta
   #:collection-class
   #:collection-slot
   ))
